
import 'package:flurdle/Data/Data.dart';
import 'package:flurdle/Logic/PaymentManager.dart';
import 'package:flurdle/Logic/Puzzle.dart';
import 'package:flutter/material.dart';
import 'package:tools/AdManager.dart';

class OutOfChecksDialog extends StatefulWidget {
	const OutOfChecksDialog(this.outOfChecks, {Key? key}) : super(key: key);

	final bool outOfChecks;

	@override
	OutOfChecksDialogState createState() => OutOfChecksDialogState();
}

class OutOfChecksDialogState extends State<OutOfChecksDialog> {

	@override
	Widget build(BuildContext context) {

		final bool canExtend = puzzle.currentMaxNumChecks < 6;
		final List<Widget> options = <Widget>[];

		if (hasProUnlock) {
			options.add(
				TextButton(
					child: const Text("YES"),
					onPressed: () {
						displayRewardAd(() {
							if (canExtend) {
								puzzle.isBoosted = true;
							} else {
								puzzle = Puzzle(day: DateTime.now().day, freePlay: true);
							}
							Navigator.pop(context);
						});
					},
				)
			);
		} else {
			options.add(
				TextButton(
					child: Row(
						children: <Widget>[
							const SizedBox(width: 5),
							Icon(Icons.movie, color: Theme.of(context).textTheme.bodyText1?.color ?? Colors.black),
							Text(" WATCH AD", style: Theme.of(context).textTheme.bodyText1)
						],
					),
					onPressed: () {
						displayRewardAd(() {
							if (!puzzle.solved && canExtend) {
								puzzle.isBoosted = true;
							} else {
								puzzle = Puzzle(day: DateTime.now().day, freePlay: true);
							}
							Navigator.pop(context);
							notifyGame();
						});
					},
				)
			);
			options.add(
				TextButton(
					child: Row(
						children: <Widget>[
							const SizedBox(width: 5),
							Icon(Icons.paid, color: Theme.of(context).textTheme.bodyText1?.color ?? Colors.black),
							Text(" BUY PRO", style: Theme.of(context).textTheme.bodyText1),
						],
					),
					onPressed: () {
						makePurchase(context).then((_) => setState(() {}));
					},
				)
			);
		}

		options.add(TextButton(
			child: Text("DISMISS", style: Theme.of(context).textTheme.bodyText1),
			onPressed: () {
				Navigator.pop(context);
			},
		));

		return AlertDialog(
			title: Center(
				child: Text(widget.outOfChecks ? "Oops No More Checks" : "Shuffle"),
			),
			content: Text((canExtend && widget.outOfChecks) ? "But don't worry. You can still get 3 more checks." : "Would you like to play again (free play game)?"),
			actions: options,
		);
	}
}
