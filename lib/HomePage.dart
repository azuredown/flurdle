
import 'dart:math';

import 'package:flurdle/Data/Data.dart';
import 'package:flurdle/Dialogs/InstructionsDialog.dart';
import 'package:flurdle/Dialogs/SettingsDialog.dart';
import 'package:flurdle/Dialogs/StatsDialog.dart';
import 'package:flurdle/Widgets/BoardDisplay.dart';
import 'package:flurdle/Widgets/BottomButton.dart';
import 'package:flurdle/Widgets/HighlightBackground.dart';
import 'package:flurdle/Widgets/StatsDisplay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tools/BasicExtensions.dart';
import 'package:tools/Startup.dart';

class HomePage extends StatefulWidget {
	const HomePage({Key? key}) : super(key: key);

	@override
	State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {

	@override
	Widget build(BuildContext context) {

		WidgetsBinding.instance!.addPostFrameCallback((_) {
			if (!prefs!.containsKey("DisplayHelp")) {
				displayHelp();
				prefs!.setBool("DisplayHelp", true);
			}
		});
		WidgetsBinding.instance!.scheduleFrame();

		final bool darkModeEnabled = Theme.of(context).darkModeEnabled;

		SystemChrome.setSystemUIOverlayStyle(darkModeEnabled ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark);

		final MediaQueryData data = MediaQuery.of(context);

		final Size size = MediaQuery.of(context).size;
		double gridSize = min(min(500, size.width * 0.75), size.height * 0.75);

		Widget getChildToBuild() {
			if (data.size.width * 0.9 > data.size.height) {
				gridSize = min(gridSize, max((size.width - 200) * 0.75, 100));
				return buildLandscape(gridSize, darkModeEnabled);
			} else {
				gridSize = min(gridSize, max((size.height - 200) * 0.75, 100));
				return buildPortrait(gridSize, darkModeEnabled);
			}
		}

		return Scaffold(
			body: SafeArea(
				child: getChildToBuild(),
			)
		);
	}

	Widget buildPortrait(double gridSize, bool darkModeEnabled) {

		final double paddingValue = min(20, MediaQuery.of(context).size.height * 0.02);

		return Column(
			children: <Widget>[
				Flexible(
					flex: 2,
					child: Center(
						child: Align(
							alignment: const Alignment(0.5, 0.25),
							child: header(paddingValue),
						),
					),
				),
				gameBoard(gridSize),
				Flexible(
					flex: 1,
					child: Center(
						child: BottomButton(gridSize * (1.5 / 4)),
					),
				),
			],
		);
	}

	Widget buildLandscape(double gridSize, bool darkModeEnabled) {

		final double paddingValue = min(20, MediaQuery.of(context).size.height * 0.02);

		return Row(
			children: <Widget>[
				const Flexible(
					flex: 3,
					fit: FlexFit.tight,
					child: SizedBox(),
				),
				Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						header(paddingValue),
						const SizedBox(height: 25),
						Transform.scale(
							scale: 0.9,
							child: BottomButton(gridSize * (1.5 / 4)),
						),
						const SizedBox(height: 20),
					],
				),
				const Flexible(
					flex: 3,
					fit: FlexFit.tight,
					child: SizedBox(),
				),
				Column(
					mainAxisSize: MainAxisSize.min,
					children: <Widget>[ gameBoard(gridSize) ],
				),
				const Flexible(
					flex: 2,
					fit: FlexFit.tight,
					child: SizedBox(),
				),
			],
		);
	}

	Widget gameBoard(double gridSize) {
		return Container(
			decoration: BoxDecoration(
				color: Theme.of(context).canvasColor.withOpacity(0.9),
				boxShadow: const <BoxShadow>[
					BoxShadow(
						color: Colors.black54,
						blurRadius: 10,
					)
				],
				border: Border.all(
					color: greyTileBackground,
					width: 7,
				),
			),
			child: Stack(
				children: <Widget>[
					HighlightBackground(gridSize),
					BoardDisplay(gridSize),
				],
			),
		);
	}

	Widget header(double paddingValue) {
		return Column(
			mainAxisAlignment: MainAxisAlignment.start,
			mainAxisSize: MainAxisSize.min,
			children: <Widget>[
				const Text("FLURDLE", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, letterSpacing: 2)),
				SizedBox(height: paddingValue),
				const StatsDisplay(),
				SizedBox(height: paddingValue),
				Row(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						IconButton(
							icon: const Icon(Icons.help_outline),
							iconSize: 40,
							tooltip: "Instructions",
							onPressed: () {
								displayHelp();
							},
						),
						IconButton(
							icon: const Icon(Icons.military_tech),
							tooltip: "Score info",
							iconSize: 40,
							onPressed: () {
								showDialog(
									context: context,
									builder: (_) {
										return const StatsDialog();
									},
								);
							},
						),
						IconButton(
							icon: const Icon(Icons.settings),
							tooltip: "Settings",
							iconSize: 40,
							onPressed: () {
								showDialog(
									context: context,
									builder: (BuildContext context) {
										return const SettingsDialog();
									},
								);
							},
						),
					],
				),
			],
		);
	}

	void displayHelp() {
		showDialog(
			context: context,
			builder: (BuildContext context) {
				return const InstructionsDialog();
			}
		);
	}
}
