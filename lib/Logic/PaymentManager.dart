
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:tools/TestUtils.dart';

const String PRO_UNLOCK_NAME = "flurdle_pro_unlock";

bool hasProUnlock = false;

/*
This is in the in app purchase documentation but it's never used so comment it out
void dispose() {
	if (!kIsWeb) {
		_purchasesStream.cancel();
	}
}
*/

// This is called whenever a purchase is made
Future<void> listenToPurchaseUpdated(List<PurchaseDetails> purchases) async {
	for (int i = 0; i < purchases.length; i++) {

		switch (purchases[i].status) {

			case PurchaseStatus.pending:
				break;
			case PurchaseStatus.canceled:
				if (purchases[i].pendingCompletePurchase) {
					// If not called purchase will be refunded after 3 days
					unawaited(InAppPurchase.instance.completePurchase(purchases[i]));
				}
				break;
			case PurchaseStatus.error:
				final IAPError? error = purchases[i].error;
				if (error == null) {
					crashlyticsRecordError("listenToPurchaseUpdated(): Encountered null error for item ${purchases[i].productID}", StackTrace.current);
				} else {

					if (error.message == "BillingResponse.userCanceled" || error.message == "BillingResponse.itemAlreadyOwned") {
						return;
					}
					// I'm not 100% sure but I think this is caused by the user cancelling the purchase
					if (error.message == "SKErrorDomain") {
						crashlyticsRecordError("SKErrorDomain Error: ${error.code} - ${error.details} - ${error.source}", StackTrace.current);
						return;
					}

					crashlyticsRecordError("listenToPurchaseUpdated(): Encountered IAP Error: $error (code: ${error.code}, message: ${error.message}, source: ${error.source}) for item ${purchases[i].productID}", StackTrace.current);
				}
				break;
			case PurchaseStatus.purchased:
			case PurchaseStatus.restored:
				await activatePurchase(purchases[i]);
				break;
		}
	}
}

Future<void> activatePurchase(PurchaseDetails purchaseDetails) async {

	if (purchaseDetails.pendingCompletePurchase) {
		// If not called purchase will be refunded after 3 days
		unawaited(InAppPurchase.instance.completePurchase(purchaseDetails));
	}

	// Apparently product id can be null if there's an error so catch that
	// Also check for legacy pro unlock name
	if (purchaseDetails.productID == PRO_UNLOCK_NAME || purchaseDetails.productID == "pro_unlock") {
		if (!hasProUnlock) {
			hasProUnlock = true;
		} else {
			crashlyticsRecordError("Duplicate activate IAP call.", StackTrace.current);
		}
	} else {
		final IAPError? error = purchaseDetails.error;
		if (error != null && (error.message.contains("BillingResponse.userCanceled") || error.message.contains("BillingResponse.itemAlreadyOwned"))) {
			return;
		}
		crashlyticsRecordError("Unknown product name: '${purchaseDetails.productID}' ($purchaseDetails, status ${purchaseDetails.status}, error: ${getError(error)})", StackTrace.current);
	}
}

String getError(IAPError? error) {
	if (error == null) {
		return "null";
	} else {
		return "IAPError(${error.code}: ${error.message}, ${error.details}, ${error.source})";
	}
}

Future<void> makePurchase(BuildContext context) async {
	//final int confirmState = config?.getInt("ConfirmState") ?? 2; // 0 = off, 1 = on but no overclock disclaimer, 2 = on with overclock disclaimer

	//if (confirmState != 0) {
	//	await showDialog(
	//		context: context,
	//		builder: (_) => PaymentConfirmPopup(
	//				_makePurchase,
	//				"- Doubles all offline income\n- Permanent overclock${(confirmState == 2 && !state.storyPointsEncountered.contains(5)) ? " (More info on story point 6)" : ""}${config?.getBool("DisplayStore") ?? false ? "\n- Store" : ""}", "Buy"),
	//		barrierDismissible: true,
	//	);
	//} else {
		await _makePurchase();
	//}
}

Future<void> _makePurchase() async {

	if (kIsWeb) {
		return;
	}

	if (!(await InAppPurchase.instance.isAvailable())) {
		unawaited(Fluttertoast.showToast(msg: "Unable to connect"));
		return;
	}

	final ProductDetails? details = await getProduct(PRO_UNLOCK_NAME);

	if (details != null) {

		// Don't have to do anything special after this finishes as it'll just push the information onto the stream
		await InAppPurchase.instance.buyNonConsumable(purchaseParam: PurchaseParam(productDetails: details));

	} else {

		unawaited(Fluttertoast.showToast(msg: "Could not find purchase"));
	}
}

Future<ProductDetails?> getProduct(String name) async {
	final Set<String> _kIds = <String>{ name };
	final ProductDetailsResponse response = await InAppPurchase.instance.queryProductDetails(_kIds);

	if (response.error != null) {
		crashlyticsRecordError("IAPError: ${response.error!.code}, ${response.error!.message}, ${response.error!.source}, ${response.error!.details}", StackTrace.current);
	}

	if (response.productDetails.length > 1) {
		crashlyticsRecordError("Found multiple items for name $name: ${response.productDetails}", StackTrace.current);
	} else if (response.productDetails.isEmpty) {
		crashlyticsRecordError("Did not find any items for name $name!", StackTrace.current);
		return null;
	}

	return response.productDetails.first;
}
