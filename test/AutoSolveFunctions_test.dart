
import 'package:flurdle/Data/Data.dart';
import 'package:flurdle/Logic/AutoSolveFunctions.dart';
import 'package:flurdle/Logic/PuzzleFunctions.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
	test('Path Finding', () {
		expect(
			pathFinding(0, <int>{}).toString(),
			"[0.0, 1.0, 2.0, 3.0, "
			"1.0, 2.0, 3.0, 4.0, "
			"2.0, 3.0, 4.0, 5.0, "
			"3.0, 4.0, 5.0, 6.0]",
		);
		expect(
			pathFinding(6, <int>{}).toString(),
			"[3.0, 2.0, 1.0, 2.0, "
			"2.0, 1.0, 0.0, 1.0, "
			"3.0, 2.0, 1.0, 2.0, "
			"4.0, 3.0, 2.0, 3.0]",
		);
		expect(
			pathFinding(6, <int>{2, 5}).toString(),
			"[5.0, 6.0, null, 2.0, "
			"4.0, null, 0.0, 1.0, "
			"3.0, 2.0, 1.0, 2.0, "
			"4.0, 3.0, 2.0, 3.0]",
		);
	});

	test('Move Hole', () {
		expect(
			moveHole(0, 1, <int>{}).toString(),
			"[Swap.RIGHT]",
		);
		expect(
			moveHole(1, 0, <int>{}).toString(),
			"[Swap.LEFT]",
		);
		expect(
			moveHole(0, 4, <int>{}).toString(),
			"[Swap.DOWN]",
		);
		expect(
			moveHole(4, 0, <int>{}).toString(),
			"[Swap.UP]",
		);
		expect(
			moveHole(6, 4, <int>{}).toString(),
			"[Swap.LEFT, Swap.LEFT]",
		);
		expect(
			moveHole(6, 4, <int>{2, 5}).toString(),
			"[Swap.DOWN, Swap.LEFT, Swap.LEFT, Swap.UP]",
		);
	});

	test('Move Single Piece', () {

		List<int?> gameBoard = <int?>[
			1, 2, 3, 4,
			5, 6, 7, 8,
			9, 10, 11, 12,
			13, 14, 15, null,
		];

		expect(
			moveSinglePiece(gameBoard.indexOf(null), 0, 0, <int>{}).length,
			0,
		);

		List<Swap> swaps = moveSinglePiece(gameBoard.indexOf(null), gameBoard.indexOf(7), 0, <int>{});
		for (int i = 0; i < swaps.length; i++) {
			applySwap(swaps[i], gameBoard.indexOf(null), gameBoard);
		}
		expect(gameBoard[0], 7);

		gameBoard = <int?>[
			1, 2, 3, 4,
			5, 6, 7, 8,
			9, 10, 11, 12,
			13, 14, 15, null,
		];

		final int positionFourteen = gameBoard.indexOf(14);
		final int positionEight = gameBoard.indexOf(8);
		swaps = moveSinglePiece(gameBoard.indexOf(null), gameBoard.indexOf(7), 0, <int>{positionFourteen, positionEight});
		for (int i = 0; i < swaps.length; i++) {
			applySwap(swaps[i], gameBoard.indexOf(null), gameBoard);
			expect(gameBoard[positionEight], 8);
			expect(gameBoard[positionFourteen], 14);
		}
		expect(gameBoard[0], 7);
	});

	/*
	test('Autosolve', () {

		final List<int?> gameBoard = <int?>[
			5, 10, 3, 4,
			6, 7, 11, 8,
			9, 1, 15, 12,
			13, 14, null, 2,
		];

		final List<int?> solution = <int?>[
			1, 2, 3, 4,
			5, 6, 7, 8,
			9, 10, 11, 12,
			13, 14, 15, null,
		];

		applySwaps(autoSolve(gameBoard, solution), gameBoard);
		print(gameBoard);
	});
	*/
}
