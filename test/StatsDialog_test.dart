
import 'package:flurdle/Dialogs/StatsDialog.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('todaysDate', () {
    expect(todaysDate(DateTime(2020, 1, 1)), "Jan 1, 2020");
    expect(todaysDate(DateTime(2020, 12, 31)), "Dec 31, 2020");
    expect(todaysDate(DateTime(2020, 1, 1, 23, 59, 59)), "Jan 1, 2020");
    expect(todaysDate(DateTime(2020, 12, 31, 23, 59, 59)), "Dec 31, 2020");
  });
}
