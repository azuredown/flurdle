# Flurdle

## A Slide Puzzle With A Twist

Flurdle applies the guessing system initially introduced in word puzzles like Wordle and applies it to a sliding puzzle.

The slide puzzle has no labels on it. The only way to know if your answer is right or wrong is to press the check button. This will tell you whether each tile is in the correct location, the correct row/column, or neither of the two options.

But be careful. You only get 3 guesses!

There are two game modes: the daily challenge mode and the free play mode. In the daily challenge mode everyone has the same puzzle to solve and the pieces start in the same order (although the icons are randomized). The free play mode acts as a generator for practice puzzles.

## Web

You need to use the HTML renderer otherwise some characters will not be displayed properly:

```
flutter run -d chrome --web-renderer=html
```

## TODO:

- Refresh the moves/checks dialog when refreshing the game
- Save the 'hole in random place' setting in the puzzle because now even if the hole isn't in a random place we can still check even if the hole isn't in the bottom right

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
